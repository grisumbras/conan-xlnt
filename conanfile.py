from conans import (
    ConanFile,
    CMake,
    tools,
)
import os
import shutil


class XlntConan(ConanFile):
    name = "xlnt"
    version = "1.3.0"
    license = "MIT"
    homepage = "https://github.com/tfussell/xlnt"
    url = "https://github.com/grisumbras/conan-xlnt"
    description = "cross-platform user-friendly xlsx library for C++14"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}

    def source(self):
        url = (
            "https://github.com/tfussell/xlnt/archive/v%s.tar.gz"
            % self.version
        )
        tools.get(url, filename="xlnt.tar.gz")

        if os.path.exists("src"):
            shutil.rmtree("src")
        shutil.move("xlnt-%s" % self.version, "src")

    def build(self):
        cmake = self._builder()
        cmake.build()

    def package(self):
        cmake = self._builder()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["xlnt"]
        self.env_info.PKG_CONFIG_PATH.append(
            os.path.join(self.package_folder, "lib", "pkgconfig")
        )

    def _builder(self):
        cmake = CMake(self)
        cmake.configure(
            source_folder=os.path.join(self.source_folder, "src"),
            build_folder=os.path.join(self.build_folder, "build"),
            defs={
                "TESTS": "OFF",
                "STATIC": "OFF" if self.options.shared else "ON",
            },
        )
        return cmake
